#!/usr/bin/env node

import chalk from "chalk";
import program from "commander";
import figlet from "figlet";
import fs from "fs";
import moment from "moment";

const fileStore = `${process.env.HOME}/.multiversary.json`;

const pjson = require("../package.json");
program.version(pjson.version);

interface Event {
  readonly name: string;
  readonly date: string;
}

function eventToString(event: Event): string {
  let date = moment(event.date);
  let weeks = Math.floor(moment().diff(date, "weeks", true));
  let weekiversary = date
    .clone()
    .add(weeks, "weeks")
    .format("LL");

  let months = Math.floor(moment().diff(date, "months", true));
  let monthiversary = date
    .clone()
    .add(months, "months")
    .format("LL");

  let years = Math.floor(moment().diff(date, "years", true));
  let anniversary = date
    .clone()
    .add(years, "years")
    .format("LL");

  let output = chalk.cyan(`## ${event.name}\n`);
  output += `* ${weeks} weeks as of ${weekiversary}\n`;
  output += `* ${months} months as of ${monthiversary}\n`;
  output += `* ${years} years as of ${anniversary}\n`;
  return output;
}

function eventIsValid(event: Event): boolean {
  let date = moment(event.date);
  return date.format() !== "Invalid date";
}

function sortEvents(events: Event[]): void {
  events.sort((a, b) => {
    return a.date.localeCompare(b.date);
  });
}

program
  .command("add <name> <date>")
  .description("Add an event to your local store, date is in YYYY-MM-DD format")
  .action((name: string, date: string) => {
    const event = { name, date } as Event;
    if (!eventIsValid(event)) {
      console.error("Invalid date, expected YYYY-MM-DD format");
      process.exit(1);
    }

    let events: Event[] = [event];
    if (fs.existsSync(fileStore)) {
      events = JSON.parse(
        fs.readFileSync(fileStore, "UTF-8").toString()
      ).concat(events);
    }
    sortEvents(events);
    fs.writeFileSync(fileStore, JSON.stringify(events), "UTF-8");
    console.log(`Saved new event: ${name} on ${date}`);
  });

program
  .command("list")
  .description("List the weekly/monthly/yearly anniversaries for saved events")
  .action(() => {
    const events: Event[] = JSON.parse(
      fs.readFileSync(fileStore, "UTF-8").toString()
    );
    sortEvents(events);
    console.log(chalk.green(figlet.textSync("multiversary", "Small")));
    events.forEach(event => console.log(eventToString(event)));
  });

// Error on unknown commands
program.on("command:*", () => {
  console.error(
    "Invalid command: %s\nSee --help for a list of available commands.",
    program.args.join(" ")
  );
  process.exit(1);
});

// Parse and run commands
program.parse(process.argv);

// Print help if no arguments are passed
if (!process.argv.slice(2).length) {
  program.outputHelp();
  process.exit(1);
}
