# 1.0.0 (2019-08-02)


### Bug Fixes

* setting version back to 0.0.1 so semantic-release can promote it ([7b924c2](https://gitlab.com/evanchiu/multiversary/commit/7b924c2))


### Features

* basic cli implementation ([0d01b90](https://gitlab.com/evanchiu/multiversary/commit/0d01b90))
* ci/cd through gitlab pipelines, semantic release ([d0b7d89](https://gitlab.com/evanchiu/multiversary/commit/d0b7d89))
