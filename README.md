# multiversary

Command line interface for tracking multiple (weekly/monthly/yearly) anniversaries

## Install

```
npm install -g multiversary
```

## Add events

```
multiversary add <name> <date>
```

## List multiversaries for added events

```
multiversary list
```
